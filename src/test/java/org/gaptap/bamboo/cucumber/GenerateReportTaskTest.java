
package org.gaptap.bamboo.cucumber;

import static org.gaptap.bamboo.cucumber.GenerateReportTaskConfigurator.COPY_PATTERN;
import static org.gaptap.bamboo.cucumber.GenerateReportTaskConfigurator.LOCATION;
import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;

/**
 * @author David Ehringer
 */
public class GenerateReportTaskTest {
    
    @Mock
    private TaskContext taskContext;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        when(taskContext.getRootDirectory()).thenReturn(new File("src/test/resources"));
    }
    
    @Test
    @Ignore
    public void test() throws TaskException{
        GenerateReportTask task = new GenerateReportTask();
        
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(LOCATION, "reports");
        configurationMap.put(COPY_PATTERN, "project1.json");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        
        task.execute(taskContext);
    }
}
