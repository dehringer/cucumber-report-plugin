/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import org.junit.Ignore;
import org.junit.Test;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * @author David Ehringer
 */
public class ViewReportActionTest {

    @Test
    public void cucumberReportUrlIsExtractedFromThePlanKeyAndBuildNumber() throws Exception {
        ViewReportAction action = new ViewReportAction();
        
        AdministrationConfiguration administrationConfiguration = mock(AdministrationConfiguration.class);
        when(administrationConfiguration.getBaseUrl()).thenReturn("http://localhost:6990/bamboo");
        action.setAdministrationConfiguration(administrationConfiguration);
        
        action.setPlanKey("AP-SUCCEED-JOB1");
        action.setBuildNumber("49");
        
        action.doDefault();

        assertThat(
                action.getReportUrl(),
                is("http://localhost:6990/bamboo/browse/AP-SUCCEED-49/artifact/JOB1/Cukes/index.html"));
    }
}
