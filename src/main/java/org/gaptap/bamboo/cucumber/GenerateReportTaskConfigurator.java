/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionImpl;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class GenerateReportTaskConfigurator extends AbstractTaskConfigurator {

    public static final String LOCATION = "cucumber_json_location";
    public static final String COPY_PATTERN = "cucumber_json_copy_pattern";

    private final PlanManager planManager;
    private final TextProvider textProvider;
    private final ArtifactDefinitionManager artifactDefinitionManager;

    public GenerateReportTaskConfigurator(TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper,
            ArtifactDefinitionManager artifactDefinitionManager,
            PlanManager planManager) {
        this.textProvider = textProvider;
        this.taskConfiguratorHelper = taskConfiguratorHelper;
        this.artifactDefinitionManager = artifactDefinitionManager;
        this.planManager = planManager;
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(
            @NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params,
                previousTaskDefinition);
        config.put(COPY_PATTERN, params.getString(COPY_PATTERN));
        config.put(LOCATION, params.getString(LOCATION));

        System.out.println("****** generateTaskConfigMap");
        for (String key : params.keySet()) {
            System.out.println("param: " + key + "=" + params.get(key));
        }

        String planKey = params.getString("planKey");
        Job job = Narrow.to(planManager.getPlanByKey(planKey, Job.class), Job.class);
        
        ArtifactDefinition artifactDefinition = new ArtifactDefinitionImpl();
        artifactDefinition.setProducerJob(job); 
        artifactDefinition.setName("Cukes");
        artifactDefinition.setLocation(params.getString(LOCATION));
        artifactDefinition.setCopyPattern("**/*");
//        artifactDefinition.setCopyPattern(params.getString(COPY_PATTERN));
        artifactDefinition.setSharedArtifact(false);
        // See ConfigureBuildArtifact
        // validateArtifact(artifactDefinition);
        artifactDefinitionManager.saveArtifactDefinition(artifactDefinition);
        
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context,
            @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context,
            @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context, taskDefinition);
    }

    private void populateContextForAll(
            @NotNull final Map<String, Object> context,
            TaskDefinition taskDefinition) {
        context.put(LOCATION, taskDefinition.getConfiguration().get(LOCATION));
        context.put(COPY_PATTERN,
                taskDefinition.getConfiguration().get(COPY_PATTERN));
    }

    @Override
    public void validate(@NotNull ActionParametersMap params,
            @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        validateRequiredNotBlank(LOCATION, params, errorCollection);
        validateRequiredNotBlank(COPY_PATTERN, params, errorCollection);
    }

    protected void validateRequiredNotBlank(String field,
            ActionParametersMap params, ErrorCollection errorCollection) {
        if (StringUtils.isBlank(params.getString(field))) {
            errorCollection.addError(field,
                    textProvider.getText("cucumber.global.required.field"));
        }
    }
}
