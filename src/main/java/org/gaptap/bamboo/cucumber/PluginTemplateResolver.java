/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import org.thymeleaf.exceptions.ConfigurationException;
import org.thymeleaf.resourceresolver.ClassLoaderResourceResolver;
import org.thymeleaf.resourceresolver.IResourceResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 * @author David Ehringer
 *
 */
public class PluginTemplateResolver extends TemplateResolver {

    public PluginTemplateResolver() {
        super();
        super.setResourceResolver(new PluginResourceResolver());
    }

    @Override
    public void setResourceResolver(final IResourceResolver resourceResolver) {
        throw new ConfigurationException("Cannot set a resource resolver on "
                + this.getClass().getName() + ". If "
                + "you want to set your own resource resolver, use "
                + TemplateResolver.class.getName() + "instead");
    }

}
