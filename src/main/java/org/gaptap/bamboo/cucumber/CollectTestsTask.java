/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import org.gaptap.bamboo.cucumber.tests.CucumberTestReportCollector;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;

/**
 * @author David Ehringer
 */
public class CollectTestsTask implements TaskType {

    private final TestCollationService testCollationService;

    public CollectTestsTask(TestCollationService testCollationService) {
        this.testCollationService = testCollationService;
    }

    @Override
    public TaskResult execute(TaskContext taskContext) throws TaskException {

        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(
                taskContext).success();

        buildLogger.addBuildLogEntry("Processing Cucumber Test Results");
        testCollationService.collateTestResults(taskContext, "target/tests/*", new CucumberTestReportCollector(), true);

        return taskResultBuilder.build();
    }

}
