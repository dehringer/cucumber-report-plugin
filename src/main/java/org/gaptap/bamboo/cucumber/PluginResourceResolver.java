/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import java.io.InputStream;

import org.thymeleaf.TemplateProcessingParameters;
import org.thymeleaf.resourceresolver.IResourceResolver;
import org.thymeleaf.util.Validate;

import com.atlassian.core.util.ClassLoaderUtils;

/**
 * @author David Ehringer
 *
 */
public class PluginResourceResolver 
    implements IResourceResolver {

        public static final String NAME = "CLASSLOADER";

        
        public PluginResourceResolver() {
            super();
        }
        

        public String getName() {
            return NAME; 
        }
        
        
        public InputStream getResourceAsStream(final TemplateProcessingParameters templateProcessingParameters, final String resourceName) {
            Validate.notNull(resourceName, "Resource name cannot be null");
            System.out.println("Looking up resource: " + resourceName);
            return ClassLoaderUtils.getResourceAsStream(resourceName, this.getClass());
        }
}
