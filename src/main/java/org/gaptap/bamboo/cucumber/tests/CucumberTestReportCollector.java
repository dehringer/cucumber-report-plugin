/**
 * 
 */
package org.gaptap.bamboo.cucumber.tests;

import java.io.File;
import java.util.Set;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author 
 *
 */
public class CucumberTestReportCollector implements TestReportCollector {

    @Override
    public TestCollectionResult collect(File file) throws Exception {
        TestCollectionResultBuilder builder = new TestCollectionResultBuilder();
        
        System.out.println("Processing Test File: " + file.getAbsolutePath() );
        
        TestResults testResults = new TestResults("Feature", "Scenario: " + file.getName(), "100");
        testResults.setState(TestState.SUCCESS);
        testResults.setSystemOut("Given\nWhen\nThen");
        
        return builder
                .addSuccessfulTestResults(Lists.newArrayList(testResults))
//                .addFailedTestResults(failingTestResults)
                .build();
    }

    @Override
    public Set<String> getSupportedFileExtensions() {
        return Sets.newHashSet("json");
    }

}
