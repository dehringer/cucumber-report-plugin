/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import java.util.Map;
import java.util.Map.Entry;

import com.atlassian.bamboo.agent.bootstrap.AgentContext;
import com.atlassian.bamboo.configuration.SystemInfo;
import com.atlassian.bamboo.migration.utils.BuildKeyHolder;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

/**
 * @author David Ehringer
 *
 */
public class ShouldDisplayWebItem implements Condition {

    @Override
    public void init(Map<String, String> arg0) throws PluginParseException {

    }

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        System.out.println("***** ShouldDisplayWebItem");
        for (Entry<String, Object> entry : context.entrySet()) {
            System.out.println("entry: " + entry.getKey() + "="
                    + entry.getValue());
        }
        return true;
    }

//    [INFO] [talledLocalContainer] ***** ShouldDisplayWebItem
//    [INFO] [talledLocalContainer] entry: planKey=AP-SUCCEED-JOB1
//    [INFO] [talledLocalContainer] entry: buildNumber=2
//    [INFO] [talledLocalContainer] entry: buildKey=AP-SUCCEED-JOB1
//    [INFO] [talledLocalContainer] entry: user=null
    
    public static void main(String[] args) {
        //http://localhost:6990/bamboo/browse/AP-SUCCEED-49/artifact/JOB1/Cukes/index.html
        BuildKeyHolder buildKeyHolder = new BuildKeyHolder("AP-SUCCEED-JOB");
//        System.out.println(buildKeyHolder.g);
    }
}
