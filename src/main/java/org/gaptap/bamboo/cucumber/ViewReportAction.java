/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import scala.collection.mutable.StringBuilder;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.spring.container.ContainerManager;

/**
 * @author David Ehringer
 */
public class ViewReportAction extends BambooActionSupport {

    private static final Pattern RESULT_KEY_PATTERN = Pattern
            .compile("((.*)-(.*))-(.*)");

    private String baseUrl;
    private String buildNumber;
    private String planKey;
    private String reportUrl;

    private AdministrationConfiguration administrationConfiguration;

    @Override
    public String doDefault() throws Exception {
        baseUrl = getAdministrationConfiguration().getBaseUrl();

        // http://localhost:6990/bamboo/browse/AP-SUCCEED-48/artifact/JOB1/Cukes/index.html
        reportUrl = new StringBuilder().append(baseUrl).append("/browse/")
                .append(getResultKey()).append("-").append(buildNumber)
                .append("/artifact/").append(getJobKey())
                .append("/Cukes/index.html").toString();

        return SUCCESS;
    }

    private Object getJobKey() {
        Matcher matcher = RESULT_KEY_PATTERN.matcher(planKey);
        if (matcher.find()) {
            return matcher.group(4);
        }
        return "";
    }

    private Object getResultKey() {
        Matcher matcher = RESULT_KEY_PATTERN.matcher(planKey);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    public void setAdministrationConfiguration(
            AdministrationConfiguration administrationConfiguration) {
        this.administrationConfiguration = administrationConfiguration;
    }

    public AdministrationConfiguration getAdministrationConfiguration() {
        if (administrationConfiguration == null) {
            return (AdministrationConfiguration) ContainerManager
                    .getComponent("administrationConfiguration");
        }
        return administrationConfiguration;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getPlanKey() {
        return planKey;
    }

    public void setPlanKey(String planKey) {
        this.planKey = planKey;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

}
