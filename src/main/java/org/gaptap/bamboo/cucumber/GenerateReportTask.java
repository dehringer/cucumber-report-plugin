/**
 * 
 */
package org.gaptap.bamboo.cucumber;

import static org.gaptap.bamboo.cucumber.GenerateReportTaskConfigurator.COPY_PATTERN;
import static org.gaptap.bamboo.cucumber.GenerateReportTaskConfigurator.LOCATION;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.masterthought.cucumber.ReportBuilder;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;

/**
 * @author David Ehringer
 */
public class GenerateReportTask implements TaskType {

    @Override
    public TaskResult execute(TaskContext taskContext) throws TaskException {

        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(
                taskContext).success();

        buildLogger.addBuildLogEntry("Generating Cucumber Report");

        File baseDir = taskContext.getRootDirectory();
        String location = taskContext.getConfigurationMap().get(LOCATION);
        String fileName = taskContext.getConfigurationMap().get(COPY_PATTERN);
        FileFinder finder = new FileFinder(new File(baseDir, location));
        List<File> matching = finder.findFilesMatching(fileName);
        // if (matching.size() != 1) {
        // StringBuilder message = new StringBuilder();
        // message.append("Found");
        // message.append(matching.size());
        // message.append("files matching the pattern ");
        // message.append(fileName);
        // message.append("in ");
        // message.append(location);
        // if (matching.size() == 0) {
        // message.append(". One matching file is expected.");
        // } else {
        // message.append(". Only one matching file is expected.");
        // }
        // throw new TaskException(message.toString());
        // }
        // File jsonReport = matching.get(0);
        // System.out.println("JSON report: " + jsonReport.getAbsolutePath());

        File reportDirectory = new File(baseDir, location);
        buildLogger.addBuildLogEntry("Report output directory: "
                + reportDirectory.getAbsolutePath());

        List<String> jsonReports = new ArrayList<String>();
        // jsonReports.add(jsonReport.getAbsolutePath());
        for (File file : matching) {
            buildLogger.addBuildLogEntry("JSON Report: "
                    + file.getAbsolutePath());
            jsonReports.add(file.getAbsolutePath());
        }
        ReportBuilder reportBuilder;
        try {
            String pluginUrlPath = "";
            String buildNumber = String.valueOf(taskContext.getBuildContext()
                    .getBuildNumber());
            String buildName = taskContext.getBuildContext().getShortName();
            boolean skipFails = false;
            boolean undefinedFails = false;
            boolean flashCharts = false;
            boolean runWithJenkins = false;
            boolean artifactEnabled = false;
            boolean highCharts = true;
            reportBuilder = new ReportBuilder(jsonReports, reportDirectory,
                    pluginUrlPath, buildNumber, buildName, skipFails,
                    undefinedFails, flashCharts, runWithJenkins,
                    artifactEnabled, "", highCharts,
                    new PluginTemplateResolver());
            reportBuilder.generateReports();
        } catch (Exception e) {
            buildLogger.addErrorLogEntry(
                    "Error occurred during report generation.", e);
            return taskResultBuilder.failedWithError().build();
        }

        return taskResultBuilder.build();
    }

}
