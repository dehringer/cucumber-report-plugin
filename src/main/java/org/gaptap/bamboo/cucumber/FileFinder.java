/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cucumber;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;

/**
 * 
 * @author David Ehringer
 */
public class FileFinder {

    private final File baseDir;

    public FileFinder(File baseDir) {
        this.baseDir = baseDir;
    }

    public List<File> findFilesMatching(String pattern) {
        if (!baseDir.exists()) {
            return new ArrayList<File>();
        }
        FileSet fileSet = new FileSet();
        fileSet.setDir(baseDir);

        PatternSet.NameEntry include = fileSet.createInclude();
        include.setName(pattern);

        DirectoryScanner scanner = fileSet.getDirectoryScanner(new Project());
        String[] srcFiles = scanner.getIncludedFiles();
        List<File> matching = new ArrayList<File>();
        for (String file : srcFiles) {
            matching.add(new File(baseDir, file));
        }
        return matching;
    }
}
